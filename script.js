// 1. На сучасних пристроях є способи ввести щось копіювати або вставити за допомогою миші.
// Тому, якщо відстежувати введення у полі <input>, то одних клавіатурних подій недостатньо.
// Події клавіатури повинні використовуватися для обробки взаємодії користувача саме з клавіатурою.
// Наприклад, якщо потрібно реагувати на стрілочні або гарячі клавішію

let btns = document.querySelectorAll(".btn");
document.addEventListener("keydown", (event) => {
  btns.forEach((element) => {
    if (element.dataset.key === event.key) {
      element.classList.add("active-btn");
    } else {
      element.classList.remove("active-btn");
    }
  });
});
